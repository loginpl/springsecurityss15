package com.haladyj.SS15RS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss15RsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss15RsApplication.class, args);
	}

}
