package com.haladyj.SS15AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss15AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss15AsApplication.class, args);
	}

}
